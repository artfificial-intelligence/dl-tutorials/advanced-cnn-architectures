# 고급 CNN 아키텍처: LeNet, AlexNet, VGG <sup>[1](#footnote_1)</sup>

> <font size="3">LeNet, AlexNet 및 VGG 같은 고급 CNN 아키텍처에 대해 알아본다.</font>

## 목차

1. [개요](./advanced-cnn-architectures.md#intro)
1. [컨볼루션 신경망(CNN)](./advanced-cnn-architectures.md#sec_02)
1. [LeNet](./advanced-cnn-architectures.md#sec_03)
1. [AlexNet](./advanced-cnn-architectures.md#sec_04)
1. [VGG](./advanced-cnn-architectures.md#sec_05)
1. [비교와 어플리케이션](./advanced-cnn-architectures.md#sec_07)
1. [요약](./advanced-cnn-architectures.md#summary)

<a name="footnote_1">1</a>: [DL Tutorial 7 — Advanced CNN Architectures: LeNet, AlexNet, VGG](https://code.likeagirl.io/dl-tutorial-7-advanced-cnn-architectures-lenet-alexnet-vgg-d475f99a5fcd?sk=6552e76c87a5343d12c17f9a2c14e445)를 편역하였다.
